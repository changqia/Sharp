import os,re
from ROOT import *

runmode = 'Cal'
runtag="Jetv02-04-17-1"
onlyNominal = False
doParallel = False
makeVarList = True
is_scale_wt = False
is_scale_ntB = False
is_mu_cali = False
is_old_inputs = False

is_fill_datapool = True
is_fill_cdipool = False


ntB_scale = {
  'up':[re.compile(r'.*SysNominal/(?!ttbar).*PxP.*$'),1.2],
  'do':[re.compile(r'.*SysNominal/(?!ttbar).*PxP.*$'),0.8],
}

wt_scale = {
  'up':[re.compile(r'.*stop_Wt.*'),1.+3.9/71.7],
  'do':[re.compile(r'.*stop_Wt.*'),1.-3.9/71.7],
}

#wps = [ '60', '70', '77', '85' ] #b-tagging wp
wps = [ '77' ] 
if runmode == 'Cal':
  mva_wps = ['85']
elif runmode == 'Trk':
  mva_wps = ['75']
else:
  print 'ERROR: check if runmod = Cal/Trk'
  exit(0)
#mva_wps =['{}'.format(a*5) for a in range(17,18)] # range(12,21)
nLoop = 10000
mcsf = 1.
#mcsf =  36.5/27.65

#path = '~/eos/atlas/user/c/changqia/BTagging/{0:}Jetv11-23-16-2'.format(runmode)
path = '/eos/atlas/user/c/changqia/BTagging/{0:}{1:}'.format(runmode,runtag)
#path = '/sps/atlas/c/cli/BTagCalibration/BTagiingCalib/TagProbeFrameWork_Full/output/{0:}{1:}'.format(runmode,runtag)
data = TFile(path+'/input.root')
mc = TFile(path+'/input.root')

xbinr = { 
          'Trk':[0,10,20,30,60,100,250],#,300], 
          'Cal':[0,20,30,60,90,140,200,300],
}
mu_wps = ['X']
if is_mu_cali:
  mu_wps = ['X','0_18','18_25','25_50']
  xbinr = { 
            'Trk':[0,20,60,300], 
            'Cal':[0,20,60,300], 
  }




xbins = xbinr[runmode]
nbins = len(xbins)-1

htemp = (mc.Get("SysNominal/ttbar_TP_1ptag2jet_MVA85_XMu_em_PxT77{0:}JetPt".format(runmode))).Clone()
htemp.Reset()

# nominal samples
MCSamples = {
              'tt'        :          ['ttbar'],
              'diboson'   :          ['diboson_sherpa'],
              'z+jets'    :          ['MadZee','MadZmumu','MadZtautau'],
              'stop'      :          ['stop_s','stop_t','stop_Wt'],
}

# mc mis-modelling
ttbarModelling = [
                  ['ttbarFragmentation',       ['ttbar'],             ['ttbarPowHW_UEEE5'] ] ,
                  ['ttbarHardScatter',         ['ttbaraMcAtNloHW'],   ['ttbarPowHW_UEEE5'] ] ,
                  ['ttbarRadiation',           ['ttbar_radhi'],       ['ttbar_radlo']    ] ,
]
stopModelling = [
                 ['StopRadiation',['stop_Wt_RadHi'],['stop_Wt_RadLo'] ] ,
                 ['StopFragmentation',['stop_Wt'],['stop_Wt_PowHer'] ] ,
]
SysModelling = {'tt':ttbarModelling,'stop':stopModelling }
