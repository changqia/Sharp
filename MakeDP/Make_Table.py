import pickle
from  itertools import *

items =  [['Cal','06','py8'],['Cal','06','py6']]
#items += [['Trk','06','py8'],['Trk','06','py6']]
items += [['Cal','04','py6']]
p_files =[ 'data/{0:}jetJetv02-{1:}-17-1__XMu{2:}.DP.pickle'.format(*keys) for keys in items ]
out = lambda x : '{0:.3f}'.format(float(x))
out_l = lambda x : '{0:.4f}'.format(float(x))

def MakeTable(data,title):
  bin_jets = {
    'Track':r'     jet $\rm p_{T}$ (GeV) & \multicolumn{1}{l}{10-20} & \multicolumn{1}{l}{20-30} & \multicolumn{1}{l}{30-60} & \multicolumn{1}{l}{60-100} & \multicolumn{1}{l}{100-250} \\ \hline',
    'Calo':r'     jet $\rm p_{T}$ (GeV) & \multicolumn{1}{l}{20-30} & \multicolumn{1}{l}{30-60} & \multicolumn{1}{l}{60-90} & \multicolumn{1}{l}{90-140} & \multicolumn{1}{l}{140-250} &\multicolumn{1}{l}{250-300} \\ '
  }
  pre = []
  pre.append(r'\begin{table}[htbp]')
  pre.append(r'\centering')
  if jet == 'Trk':
    pre.append(r'\begin{tabular}{lrrrrr}')
  else:
    pre.append(r'\begin{tabular}{lrrrrrr}')
  pre.append(r'  \toprule')
  post = []
  post.append(r'    \bottomrule')
  post.append(r'    \end{tabular}')
  post.append('\caption*{{{0:}}}'.format(title))
  post.append(r'\end{table}')
  for line in pre:
    print line
  hline()
  print bin_jets[jet]
  hline()
  PrintRow('scale factors',data['sf'],out)
  hline()
  PrintRow('modelling syst.',data['modsys'],out)
  PrintRow('detector syst',data['detsys'][1:],out)
  PrintRow('mc stat.',data['mcstat'],out)
  PrintRow('Wt norm.',data['norm wt'][1:],out_l)
  PrintRow(r'none $t\bar{t}$ norm.',data['norm ntB'][1:],out)
  hline()
  PrintRow('syst.',data['sys'],out)
  PrintRow('stat',data['stat'],out)
  hline()
  PrintRow('total uncer.',data['total'],out)

  for line in post:
    print line

def PrintRow(name,row,out):
  line = name
  for item in row:
#    name += ' & {0:^3} '.format(1)
    name += ' & {0:} '.format(out(item))
  name += r'\\'
  print name

def hline():
  print r'\hline'    
    
def main():
  with open(p_file,'rb') as f:
    Data = pickle.load(f)[mva]
  for key,value in Data.iteritems():
    print '% SPLIT {0:}  {1:} mv2c10 '.format(mva,key)
    title = r'{0:} jets calibration at {1:} WP, Lumi = {2:}$\rm fb^{{-1}}$, used {3:} $t\bar{{t}}$ sample'.format(jet,key,lumi,tt) 
    MakeTable(value,title)

if __name__ == '__main__':
  
  for key in ['60','70','77','85']:
    for p_file in p_files:
      mva = '85MVA' if 'Cal' in p_file else '75MVA'
      with open(p_file,'rb') as f:
        Data = pickle.load(f)[mva]
      if 'py8' in p_file:
        tt = 'Pythia 8'
      else:
        tt = 'Pythia 6'
      if '02-06-17' in p_file:
        lumi = '36.5'
      else:
        lumi = '27.65'
      if 'Cal' in p_file:
        jet = 'Calo'
      else:
        jet = 'Track'
      print '% SPLIT {0:}  {1:} mv2c10 '.format(mva,key)
      title = r'{0:} jets calibration at {1:} WP, Lumi = {2:}$\rm fb^{{-1}}$, used {3:} $t\bar{{t}}$ sample'.format(jet,key,lumi,tt) 
      MakeTable(Data[key],title)
