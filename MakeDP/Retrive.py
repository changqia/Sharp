#!/usr/bin/python
import sys, time
import Cfg as C
import ROOT as R
import pickle
import re,copy,array,operator,functools,math,itertools,multiprocessing

#don't read these functions, not interesting
def Dec(fun):
  @functools.wraps(fun)
  def fun_wrapper(*args,**kw):
    return list(fun(*args,**kw))
  return fun_wrapper
def update(x,y):
  z=copy.deepcopy(x)
  z.update(y)
  return z
def div(x,y):
    try:
        a = x/y
    except ZeroDivisionError:
        a = 0.0
    return a

#used to store data, same as TH1F, but lighter than
class Hist:
  def __init__(self,th1=None,bincontent=None,binerror=None):
    self.bincontent = []
    self.binerror = []
    self.stat = None
    if th1 != None:
      for i in range(0, th1.GetNbinsX()):
        self.bincontent.append(th1.GetBinContent(i+1))
        self.binerror.append(th1.GetBinError(i+1))
    elif bincontent!=None:
        self.bincontent.extend(bincontent)
        if binerror!=None:
          self.binerror.extend(binerror)
  def Getnbins(self):
    return len(self.bincontent)
  def Clear(self,v=0):
    self.binerror=[0]*self.Getnbins()
    self.bincontent=[v]*self.Getnbins()
  def sqrt(self):
    return Hist(bincontent = map(math.sqrt,self.bincontent))
  def __add__(self,other):
    return Hist(bincontent=map(operator.add,self.bincontent,other.bincontent))
  def __sub__(self,other):
    return Hist(bincontent=map(operator.sub,self.bincontent,other.bincontent))
  def __mul__(self,other):
    return Hist(bincontent=map(operator.mul,self.bincontent,other.bincontent))
  def __div__(self,other):
    if isinstance(other,Hist):
      return Hist(bincontent=map(div,self.bincontent,other.bincontent))
    else:
      return Hist(bincontent=map(lambda x:x/other,self.bincontent))
  def __pow__(self,n):
    return Hist(bincontent=map(lambda x:x**n,self.bincontent))


#returen a map(dict) which connect a key in lis to sum of fmts histos
#input: fmts = ['WW...{reg}','WZ...{reg}',...]  lis = ['PxP','PxT'...]
#output: {'PxT':Hist, 'PxP':Hist, ... } 
def RetrieveH(fmts,lis,tfile,scale_info=None):
  #Get the sum of a list(hnames) of histograms in 'tfile', could scale or rebin it
  def Gethist(tfile,hnames,scale_info=None):
    h = C.htemp.Clone()
    for hname in hnames:
      temp =  tfile.Get(hname)
      if temp == None:
        print 'warning:  {0:} is not exist'.format(hname)
        continue
      if (scale_info != None) and scale_info[0].match(hname):
        print 'match!!!!',hname
        print 'scale : ',scale_info[1]
        temp.Scale(scale_info[1])
      h.Add(temp)
    xbins = array.array('d',C.xbins)
    h = h.Rebin(C.nbins,'',xbins)
    h.SetBinContent(1,0.)
    h.SetBinError(1,0.)
    if hnames[0].find('data')==-1:
      h.Scale(C.mcsf)
    return Hist(h)
  return { cat:Gethist(tfile,map(lambda x: x.format(cat),fmts),scale_info) for cat in lis }


#input:  fmt = '...{sam}...{{reg}}...' sam_lis = ['ZZ','WW','WZ'] reg_lis=['PxT','PxP'...]  
#output: {'PxT':Hist , 'PxP':Hist , ...}
def Retrieve(fmt,sam_lis,reg_lis,tfile,scale_info=None):
  fmts = map(lambda x:fmt.format(x),sam_lis)
  return RetrieveH(fmts,reg_lis,tfile,scale_info)

#input:
#  fmt = '...{sam}...{{reg}}...'  samples = {'ttbar':['ttbar'],'stop':['stopWt','stop_t'...],...}
#output:[ref_raw] 
#{
#  'tt':   {'PxP':Hist,  'PxT':Hist, ...}
#  'stop': {'PxP':Hist,  'PxT':Hist, ...},
#  'other':{'PxP':Hist,  'PxT':Hist, ...},
#}
def RetrieveMC(fmt,samples,scale_info=None):
  sam_o = []
  for sam in samples:
    if sam != 'tt' and sam != 'stop':
      sam_o = sam_o + samples[sam]
  rawmc = {}
  rawmc['tt'] = Retrieve(fmt,samples['tt'],['PxT','PxP','PbT','PbP','PjT','PjP'],C.mc,scale_info)
  rawmc['stop'] = Retrieve(fmt,samples['stop'],['PxT','PxP'],C.mc,scale_info)
  rawmc['other'] = Retrieve(fmt,sam_o,['PxT','PxP'],C.mc,scale_info)
  return rawmc


#Random Generator for re-sampling ['PxP','PxT',...]
#input: raw = [ref_raw], wine = TRandom3()
#output:[ref_raw]
def Drunkwalker(raw,wine):
  def pwalker(tot,pas):
    Rtot = map(wine.Poisson,tot.bincontent)
    round_Rtot = map(lambda x:int(round(x)),Rtot)
    Rpas = map(wine.Binomial,round_Rtot,(pas/tot).bincontent)
    return Hist(bincontent=Rtot),Hist(bincontent=Rpas)
  def gwalker(tot,pas):
    Rtot = map(wine.Gaus,tot.bincontent,tot.binerror)
    ratio = (pas/tot).bincontent
    gerr = map(lambda x,y:x*math.sqrt(y*(1-y)),tot.binerror,ratio)
    Rpas = map(lambda x,y,z:wine.Gaus(x*y,z),Rtot,ratio,gerr)
    return Hist(bincontent=Rtot),Hist(bincontent=Rpas)
  Rraw = {'dt':{},'tt':{},'stop':{},'other':{}}
  Rraw['dt']['PxT'],Rraw['dt']['PxP'] = pwalker(raw['dt']['PxT'],raw['dt']['PxP'])
  Rraw['tt']['PxT'],Rraw['tt']['PxP'] = gwalker(raw['tt']['PxT'],raw['tt']['PxP'])
  Rraw['tt']['PbT'],Rraw['tt']['PbP'] = gwalker(raw['tt']['PbT'],raw['tt']['PbP'])
  Rraw['tt']['PjT'],Rraw['tt']['PjP'] = gwalker(raw['tt']['PjT'],raw['tt']['PjP'])
  Rraw['stop']['PxT'],Rraw['stop']['PxP'] = gwalker(raw['stop']['PxT'],raw['stop']['PxP'])
  Rraw['other']['PxT'],Rraw['other']['PxP'] = gwalker(raw['other']['PxT'],raw['other']['PxP'])
  return Rraw

#input:[ref_raw]
#output:[ref_dish]
#  {'sf':Hist', e_tt':Hist, 'f_tb':Hist }
def Cook(raw):
  dt = raw['dt']
  tt = raw['tt']
  nt = {}
  for key in raw['stop']:
    nt[key] = raw['stop'][key]+raw['other'][key]
  dish = {}
  dish['e_tt'] = (dt['PxP']-nt['PxP'])/(dt['PxT']-nt['PxT'])
  dish['f_tb'] = tt['PbT']/tt['PxT']
  dish['e_nb'] = tt['PjP']/tt['PjT']
  dish['e_mc'] = tt['PbP']/tt['PbT'] 
  I = Hist(bincontent=[1]*(dish['f_tb'].Getnbins()))
  dish['e_dt'] = (dish['e_tt']-dish['e_nb']*(I-dish['f_tb']))/dish['f_tb']
  dish['sf'] = dish['e_dt']/dish['e_mc']
  return dish

#Get stat error by resampling
#input: raw = [ref_raw]  walker = Drunkwalker
#output:[ref_stat] { 'dt':[ref_dish] 'mc':[ref_dish] }  
def StatCalculator(raw,walker):
  holder = {'dt':{},'mc':{}} #dt/mc->e_tt/f_tb/.../->m1/m2
  r = R.TRandom3()
  for i in xrange(C.nLoop):
    Rraw = walker(raw,r)
    dtraw = update(raw,{'dt':Rraw['dt']})
    mcraw = update(Rraw,{'dt':raw['dt']})
    Rdish = {  'dt':Cook(dtraw), 'mc':Cook(mcraw)  }
    for key in Rdish:
      for item in Rdish[key]:
        if item not in holder[key]:
          holder[key][item]={'m1':Rdish[key][item]/C.nLoop,'m2':(Rdish[key][item]**2)/C.nLoop}
        else:
          holder[key][item]['m1']+=(Rdish[key][item]/C.nLoop)
          holder[key][item]['m2']+=(Rdish[key][item]**2)/C.nLoop
  disherr={}
  for stat in Rdish:
    disherr[stat]={}
    for key in Rdish[stat]:
      var=(holder[stat][key]['m2']-holder[stat][key]['m1']**2).bincontent
      var=map(lambda x:0.0 if x>-1e-12 and x<1e-12 else x,var)
      disherr[stat][key] = Hist(bincontent=map(math.sqrt,var))
  return disherr

#Get the detector related systematic items from the TDirectories in the mc input file.
#output: {'EL_MUON_SYS': ['EL_MUON_SYS_1down', 'EL_MUON_SYS_1up'], ... } 
def Getvars(tfile):
  systs_var = {}
  ptn = re.compile(r'_*1*up$|_*1*down$')
  for i in tfile.GetListOfKeys():
    obj = i.ReadObj()
    name = obj.GetName()
    cls_name = obj.ClassName()
    if name.find('Nominal')!=-1 or cls_name.find('TDirectory')==-1:
      continue
    if name.find('MUON_EFF_Trig')!=-1:
      continue
    key = ptn.split(name)[0]
    if (key in systs_var):
      systs_var[key].append(name)
    else:
      systs_var[key] = [name]
  for key in systs_var: 
    if len(systs_var[key])==1:
      systs_var[key].append('Nominal')
  return systs_var

#Calculate the detector related systematics 
#output:[ref_syst] { 'EL_MUON_SYS':[ref_dish], ... }
def SystCalculator(dt,systs):
  systs_err = {}
  for item in systs:
    raw_pair = [ update(systs[item][key],{'dt':dt}) for key in [0,1] ]
    systs_err[item] = DifferCalculator(raw_pair)
  return systs_err

#Calculate the systematics from mc mis-modelling
#input: nominal = [ref_dish]  
#input: models = { 
#  'stop':{'radioation':[ref_dish, ref_dish],  'fragmetation':[...]},
#  'ttbar':... 
#}
#output[ref_model]:
#{
#  'stop':{'redioation':[ref_dish], 'fragmetation':[ref_dish], ... },
#  'ttbar':{...},
#}
def ModellingCalculator(nominal,models):
  modelling_err = {}
  for mod in models:
    modelling_err[mod]={}
    for item in models[mod]:
      raw_pair = [ update(nominal,{mod:models[mod][item][key]}) for key in [0,1] ]
      modelling_err[mod][item] = DifferCalculator(raw_pair)
  return modelling_err

def DifferCalculator(raw_pair):
  up = Cook(raw_pair[0])
  do = Cook(raw_pair[1])
  differ = {}
  for key in up:
#    print key,'up',up[key].bincontent
#    print key,'do',do[key].bincontent
    differ[key] = Hist(bincontent=map(abs,(up[key]/2-do[key]/2).bincontent))
  return differ

#input: args = [mva_wp,mv2c10_wp]
#output: (results, mva_wp,mv2c10_wp)
#results = {
#  'nominal':[ref_dish],
#  'stat':[ref_stat],
#  'syst':[ref_syst],
#  'model':[ref_model],
#}
def GetResults(args):
  mva_wp = args[0]
  wp = args[1]
  mu_wp = args[2]
  print ('mva {0:} wp, mv2c10 {1:} wp started'.format(mva_wp,wp)).center(40)
  fmt_dt = r'SysNominal/data_TP_1ptag2jet_MVA'+mva_wp+mu_wp+'_em_{}'+wp+C.runmode+'JetPt'
  fmt_mc = r'SysNominal/{}_TP_1ptag2jet_MVA'+mva_wp+mu_wp+'_em_{{}}'+wp+C.runmode+'JetPt'
  fmt_sys = r'{0}/{{}}_TP_1ptag2jet_MVA'+mva_wp+mu_wp+'_em_{{{{}}}}'+wp+C.runmode+'JetPt_{0}'
  raw_nominal = {}
  raw_nominal['dt'] = RetrieveH([fmt_dt],['PxT','PxP'],C.data)
  raw_nominal.update(RetrieveMC(fmt_mc,C.MCSamples))
  raw_modelling = {} #tt/stop->[up(->PxP,PxT),do]
  for sam in C.SysModelling:
    reglis = ['PxT','PxP']
    if sam == 'tt':
      reglis.extend(['PbT','PbP','PjT','PjP'])
    raw_modelling[sam] = {}
    for sysitem in C.SysModelling[sam]:
      raw_up = Retrieve(fmt_mc,sysitem[1],reglis,C.mc)
      raw_do = Retrieve(fmt_mc,sysitem[2],reglis,C.mc)
      raw_modelling[sam][sysitem[0]] = [raw_up,raw_do]
  raw_sys = {}
  if not C.onlyNominal:
    for sys in systs:
      raw_sys[sys]=[]
      for item in systs[sys]:
        if item.find('Nominal')!=-1:
          tmp=fmt_mc
        else:
          tmp=fmt_sys.format(item)
        sys_updo = RetrieveMC(tmp,C.MCSamples)
        raw_sys[sys].append(sys_updo)
  dish_nominal = Cook(raw_nominal)
  stat = StatCalculator(raw_nominal,Drunkwalker)
  if not C.onlyNominal: syst = SystCalculator(raw_nominal['dt'],raw_sys);
  modelling = ModellingCalculator(raw_nominal,raw_modelling)
  if not C.onlyNominal: results = {'nominal':dish_nominal,'stat':stat,'syst':syst,'model':modelling,'raw':raw_nominal};
  else: results = {'nominal':dish_nominal,'stat':stat,'model':modelling};
  #Wt scale and nonettbarB scale
  if C.is_scale_wt: 
    raw_wt_scale = [ update(raw_nominal,RetrieveMC(fmt_mc,C.MCSamples,C.wt_scale[key])) for key in ['up','do'] ]
    wt_scale = DifferCalculator(raw_wt_scale) 
    results.update({'norm wt':wt_scale})
  if C.is_scale_ntB:
    raw_ntB_scale = [ update(raw_nominal,RetrieveMC(fmt_mc,C.MCSamples,C.ntB_scale[key])) for key in ['up','do'] ]
    ntB_scale = DifferCalculator(raw_ntB_scale)
    results.update({'norm ntB':ntB_scale})
  return results,mva_wp,wp  

def Fun(mu_wp):
  if C.doParallel:
    print C.mva_wps
    print C.wps
    variable_list=itertools.product(C.mva_wps,C.wps,[mu_wp])
    results_gene = multiprocessing.Pool(4).map(GetResults,variable_list)
    totResults = {}
    for result in results_gene:
      if not result[1] in totResults:
        totResults[result[1]]={}
      totResults[result[1]][result[2]] = result[0]
  else:
    totResults={}
    for mva_wp in C.mva_wps:
      totResults[mva_wp]={}
      for wp in C.wps:
        totResults[mva_wp][wp]=GetResults([mva_wp,wp,mu_wp])[0]
 
  #doesn't interesting
  out = lambda x:map('{:.3f}'.format,x)[1:]
  out_l = lambda x:map('{:.5f}'.format,x)[1:]
  getsum2 = lambda x: reduce(lambda _x,_y:(_x**2+_y**2).sqrt(),x)
  sum2 = lambda x: reduce(lambda a,b:math.sqrt(a**2+b**2),x)
  
  #interface functions to read info from totResults[mva_wp][mv2c10_wp]
  #all these functions return python list
  #x = 'sf','e_nonb','f_tb'.... for all these functions 

  nominal = lambda x:results['nominal'][x].bincontent 

  stat = lambda x,y:results['stat'][y][x].bincontent #y = 'mc','dt'

  model = lambda x,y:{key:results['model'][y][key][x] for key in results['model'][y]} #y = 'tt','stop

  detecsum = lambda x:getsum2([results['syst'][key][x] for key in results['syst']]).bincontent
  
  modelsum_x = lambda x,y:getsum2([results['model'][y][key][x] for key in results['model'][y]]).bincontent #y = 'tt','stop'
  
  modelsum_X = lambda x:map(sum2,zip(modelsum_x(x,'tt'),modelsum_x(x,'stop'))) 

  #get dict
  detec_dict = lambda x:{key:results['syst'][key][x].bincontent for key in results['syst']}
  mod_dict = lambda x,y:{'mod_%s_%s'%(y,key):results['model'][y][key][x].bincontent for key in results['model'][y]}
  
  if C.is_scale_wt:
    norm_wt = lambda x: results['norm wt'][x].bincontent
  if C.is_scale_ntB:
    norm_ntB = lambda x: results['norm ntB'][x].bincontent

  if not C.onlyNominal: 
    systsum = lambda x:map(sum2,zip(modelsum_X(x),detecsum(x),stat(x,'mc')))
    if C.is_scale_wt:
      toterr = lambda x:map(sum2,zip(modelsum_X(x),detecsum(x),stat(x,'mc'),stat(x,'dt'),norm_wt(x)))
    else:
      toterr = lambda x:map(sum2,zip(modelsum_X(x),detecsum(x),stat(x,'mc'),stat(x,'dt')))
  else: 
    systsum = lambda x:map(sum2,zip(modelsum_X(x),stat(x,'mc')))
    if C.is_scale_wt:
      toterr = lambda x:map(sum2,zip(modelsum_X(x),stat(x,'mc'),stat(x,'dt'),norm_wt(x)))
    else:
      toterr = lambda x:map(sum2,zip(modelsum_X(x),stat(x,'mc'),stat(x,'dt')))
  #Fill info to pickle files.
  DataPool={}
  CDIPool = {}
  for mva_wp in C.mva_wps:
    DataPool['{}MVA'.format(mva_wp)]={}
    for wp in C.wps:
      results=totResults[mva_wp][wp]
      #validation printing
      print '\n\n','firt look at the results:'
      print 'mva ',mva_wp,' mv2c10 ',wp,' sf      ',out(nominal('sf'))
      print 'mva ',mva_wp,' mv2c10 ',wp,' mc stat ',out(stat('sf','mc'))
      print 'mva ',mva_wp,' mv2c10 ',wp,' dt_stat ',out(stat('sf','dt'))
      print 'mva ',mva_wp,' mv2c10 ',wp,' model   ',out(modelsum_X('sf'))
      if not C.onlyNominal: 
        print 'mva ',mva_wp,' mv2c10 ',wp,' detec   ',out(detecsum('sf'))
      print 'mva ',mva_wp,' mv2c10 ',wp,' tot err ',out(toterr('sf'))
      if C.is_scale_wt:
        print 'mva ',mva_wp,' mv2c10 ',wp,' norm wt ',out_l(norm_wt('sf'))
      if C.is_scale_ntB:
        print 'mva ',mva_wp,' mv2c10 ',wp,' norm ntB ',out(norm_ntB('sf'))
      #fill DataPool for plotting
      DataPool['{}MVA'.format(mva_wp)][wp]={
			  'sys':out_l(systsum('sf')),
        'modsys':out_l(modelsum_X('sf')),
			  'stat':out_l(stat('sf','dt')),
        'mcstat':out_l(stat('sf','mc')),
			  'total':out_l(toterr('sf')),
			  'sf':out_l(nominal('sf')),
			  'bf':out_l(nominal('f_tb')),
	  	  'bf stat':out_l(stat('f_tb','mc')),

			  'e_dtsys':out_l(systsum('e_dt')),
        'e_dtmodsys':out_l(modelsum_X('e_dt')),
			  'e_dtstat':out_l(stat('e_dt','dt')),
        'e_dtmcstat':out_l(stat('e_dt','mc')),
			  'e_dttotal':out_l(toterr('e_dt')),
			  'e_dte_dt':out_l(nominal('e_dt')),


			  'e_mcsys':out_l(systsum('e_mc')),
        'e_mcmodsys':out_l(modelsum_X('e_mc')),
			  'e_mcstat':out_l(stat('e_mc','dt')),
        'e_mcmcstat':out_l(stat('e_mc','mc')),
			  'e_mctotal':out_l(toterr('e_mc')),
			  'e_mc':out_l(nominal('e_mc')),
	    }
      results=totResults[mva_wp][wp]
      if (C.runmode == 'Cal' and mva_wp=='85') or (C.runmode == 'Trk' and mva_wp=='75'):
        dec = detec_dict('sf')
        mod_tt = mod_dict('sf','tt')
        mod_st = mod_dict('sf','stop')
        njets = results['raw']['dt']['PxT'].bincontent
        ntagged = results['raw']['dt']['PxP'].bincontent
        CDIPool[wp] = {
			    'sf':nominal('sf'),
			    'stat':stat('sf','dt'),
          'sys':systsum('sf'),
          'total':toterr('sf'),
          'systs':update({'mc_stat':stat('sf','mc')},update(dec,update(mod_tt,mod_st))),
          #'systs':update(detec_dict('sf'),update(mod_dict('sf','tt'),mod_dict('sf','stop'))),
          'detecsys':dec,
          'mod':update(mod_tt,mod_st),
          'n_jets':njets,
          'n_tagged':ntagged,
        }
        
      if not C.onlyNominal: DataPool['{}MVA'.format(mva_wp)][wp].update({'detsys':detecsum('sf')})
      if C.is_scale_wt: DataPool['{}MVA'.format(mva_wp)][wp].update({'norm wt':norm_wt('sf')})
      if C.is_scale_ntB: DataPool['{}MVA'.format(mva_wp)][wp].update({'norm ntB':norm_ntB('sf')})
  if C.is_fill_datapool: 
    #with open('../data/{0:}jet{1:}.DP.pickle'.format(C.runmode,C.runtag),'wb') as f:
    #with open('data/{0:}jet{1:}_{2:}.DP.pickle'.format(C.runmode,C.runtag,mu_wp),'wb') as f:
    with open('../data/{0:}jet{1:}_{2:}.DP.pickle'.format(C.runmode,C.runtag,mu_wp),'wb') as f:
      pickle.dump(DataPool,f)
      print '\n\n\npicke file generated:\n','data/{0:}jet{1:}_{2:}.DP.pickle\n\n'.format(C.runmode,C.runtag,mu_wp)
  if C.is_fill_cdipool: 
    #with open('../data/{0:}jet{1:}.CP.pickle'.format(C.runmode,C.runtag),'wb') as f:
    #with open('data/{0:}jet{1:}_{2:}.CP.pickle'.format(C.runmode,C.runtag),mu_wp,'wb') as f:
    with open('../data/{0:}jet{1:}_{2:}.CP.pickle'.format(C.runmode,C.runtag),mu_wp,'wb') as f:
      pickle.dump(CDIPool,f)
      print '\n\n\npicke file generated:\n','data/{0:}jet{1:}_{2:}.CP.pickle\n\n'.format(C.runmode,C.runtag,mu_wp)
def main():
    if C.is_mu_cali:
      mu_wps = ['_{0:}Mu'.format(mu) for mu in C.mu_wps]
    elif C.is_old_inputs:
      mu_wps = ['']
    else:
      mu_wps = ['_XMu']
    for mu_wp in mu_wps:
      Fun(mu_wp)
if __name__ == '__main__':
  map=Dec(map)
  zip=Dec(zip)
  if C.makeVarList:
    systs = Getvars(C.mc)
    with open('../data/var.pickle','wb') as f:
      pickle.dump(systs,f)
  else:
    with open('../data/var.pickle','rb') as f:
      systs = pickle.load(f)
  main()
