#!/usr/bin/python
import sys
import pickle
from ROOT import *

# import the plotting tool
sys.path.append('AnalysisToolKit/plotting')
import comparison as P

if __name__ == '__main__':
  #Jet = 'Calo Jets'
  Jet = 'TrackJets'
  P.lumi = '36.5'
  P.DrawL = False
  wps = ['77']
  #wps = ['60','70','77','85']
  for wp in wps:
    DP_File = {
      'Calo Jets':
      {
      #'28 fb^{-1} t#bar{t} Pythia6': 'CaljetJetv02-04-17-1__XMupy6.DP.pickle',
      '37 fb^{-1} t#bar{t} Pythia6': '../data/CaljetJetv02-20-17-1_MVA100_XMupy6.DP.pickle',
      '37 fb^{-1} t#bar{t} Pythia8': '../data/CaljetJetv02-20-17-1_MVA100_XMupy8.DP.pickle',
      },
      # track jets
      'TrackJets':
      {
      '37 fb^{-1} t#bar{t} Pythia6': '../data/TrkjetJetv02-20-17-1_MVA100_XMupy6.DP.pickle',
      '37 fb^{-1} t#bar{t} Pythia8': '../data/TrkjetJetv02-20-17-1_MVA100_XMupy8.DP.pickle',
      },
    }
    DATA = {}
    DATA[Jet] = {}
    for key,value in DP_File[Jet].iteritems():
      with open(value,'rb') as f:
        DATA[Jet][key] = pickle.load(f)['100MVA']
    P.MethodSet.clear()
    P.PlotTitle = 'Comparison between dataset and ttbar generator'
    P.Central['width']=1800
    P.Central['LegColumns']=3
    P.Central['LP']=[0.35,0.60,0.75,0.9]
    P.Central['OutName']='sf_compare_mva100'
    P.Central['Status']='Internal'
    P.status_x = 0.26
    P.status_y = 0.85
    P.Central['LabSize']=0.1
    P.Central['xTitle'] = 'P_{T}(jet) [GeV]'
    P.Central['yTitle'] = '#varepsilon_{b}^{data}/#varepsilon_{b}^{MC}'
    P.Central['yOffset'] = 0.7 #0.9
    P.Central['xOffset'] = 1.5 #1.3
    P.MethodSet = {
          '37 fb^{-1} t#bar{t} Pythia6':{'line':-1,'shift':0,  'colo':kBlue},
          '37 fb^{-1} t#bar{t} Pythia8':{'line':-1,'shift':1,  'colo':kRed},
          #'28 fb^{-1} t#bar{t} Pythia6':{'line':-1,'shift':2,  'colo':kGreen},
    }
    P.ErrSet  = {
          'total':{'colo':-1,'pos':0, 'line':1},
            'sys':{'colo':-1,'pos':1, 'line':2},
           'stat':{'colo':-1,'pos':2, 'line':3},
    }
    P.MakePlot(Jet,wp,DATA)
