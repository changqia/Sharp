#ifndef _DrawBox_h_
#define _DrawBox_h_
#define length(array) sizeof(array)/sizeof(*array)
#include "AtlasStyle.C"
#include "setStyleAtlas.h"
#include <iostream>
#include <map>
#include <limits>
#include <vector>
#include <algorithm>
#include <boost/format.hpp>
#include <TH1F.h>
#include <TFile.h>
#include <THStack.h>
#include <TLegend.h>
using namespace std;

void PH(TH1F* h,string info){}
namespace UserDef
{
  enum TYPE {MC_COM, MC_SUM,DATA,};
}


class DrawBox
{
  
 protected:
  enum MC_TYPE      {SINGLE_KEY, DOUBLE_KEY};
  enum REG_TYPE     {NO_REG ,    MULTI_REGS = 2};
  enum REBIN_TYPE   {NOREBIN,        MERGE,      RESPLIT,    RERANGE,    MERGE_RERANGE}; 
  enum SAMPLE_TYPE  {MC,    DATA};
  struct LP
  {
    float x1 = 0.7;
    float y1 = 0.75;
    float x2 = 0.85;
    float y2 = 0.95;
  };
  struct SampleEntry
  {
    string name;
    string key;
    vector<string> sub;
    int color;
    float sf;
    SAMPLE_TYPE type;
    TFile* file;
  };
  struct Variable
  {
    string name;
    string xtitle;
    string ytitle;
    bool   log_scale;
    int nbins;
    double *xbins;
    float lowerL;
    float upperL;
    float funit;
    REBIN_TYPE  rebin_type;
    string hs_ytitle;
  };
  struct Text
  {
    float x;
    float y;
    Color_t color;
    string text;
    float size;
  };

 public:
  //functiions
  DrawBox(TFile * file);
  DrawBox(TFile * dt_file,TFile * mc_file);
  
  void ResetA();
  void ResetP();
  void AddLocalText(float x,float y,Color_t color,string text,float size = -1.);
  void AddGlobeText(float x,float y,Color_t color,string text,float size = -1.);
  void Set_ATLAS_LABEL(float x,float y,Color_t color);
  void SetLegendPosition(float x_min,float y_min,float x_len, float y_len);
  //1 key and add key in one time
  void AddMC(string name,vector<string> keys,int color, float sf = 1.);
  void AddMC(string name,vector<string> keys,int color,TFile*, float sf = 1.);
   
  //2 key and add keys in one time
  void AddMC(string name,string key,vector<string> keys,int color, float sf = 1.);
  void AddMC(string name,string key,vector<string> keys,int color,TFile* f, float sf = 1.);
  
  //1 key and add keys one by one
//  void AddMC(string name,string key,int color,float sf = 1.);
   
  void AddData(string name,string key,int color, float sf = 1.);
  
  //MULTI REGS add one by one
  void AddRegion(string reg);

  //MULTI REGS add in one time
  void AddRegion(vector<string> regs);

  //add var: RESPLIT
  void AddVariable(string var,string xtitle,string ytitle,bool log_scale,double *xbins,int nbins,float funit );
  //add var: RERANGE
  void AddVariable(string var,string xtitle,string ytitle,bool log_scale,float lowerL,float upperL,float funit = -1.);
  //add var: NOREBIN or MERGE or MERGE+RERANGE
  void AddVariable(string var,string xtitle,string ytitle,bool log_scale,int nbins = -1,float lowerL=NAN, float upperL=NAN,float funit = -1.);

  TH1F* GetHist(SampleEntry entry);
  //uniform fmt 
  void SetFormat(string fmt);

  //different fmt btw dt&mc
  void SetFormat(string dt_fmt,string mc_fmt);
  
  void SetSaveType(vector<string> save_type);
  void SetSavePath(string path);
  void SetFile(TFile *df ,TFile *mf);
  void Draw();
  void DrawPic();

  void SetLocalDraw(bool draw_local){m_draw_local = draw_local;};
  void SetUserProcess( TH1F* (*fun)(TH1F*,UserDef::TYPE) );
  void SetFileTag(string ftag){m_ftag = ftag;};
 protected:
  //functions
  void PrintYields();
  string GetName(SAMPLE_TYPE type,string key,string sub="");
  void   GetPlots();
  void FillVariable(string name,string xtitle,string ytitle,bool log_scale,int nbins,double *xbins,float lowerL,float upperL,float funit,REBIN_TYPE rebin_type);
  void Assert(void *, string info);
  TH1F* Process(TH1F *h, SampleEntry entry);
  void DrawText(Double_t x,Double_t y,Color_t color, const char *text, Double_t tsize);
  
  TH1F* (*UserProcess)(TH1F*,UserDef::TYPE type) = [](TH1F* h,UserDef::TYPE type){return h;};
  //memebers
  map<string, TH1F *>    m_current_mc_hists;
  TH1F *                 m_current_sum_hist; 
  TH1F *                 m_current_data_hist; 
  
  LP                     m_lp;
  vector<Text>           m_globe_texts;
  vector<Text>           m_local_texts;
  Text                   m_atlas_label;
  MC_TYPE                m_mc_type;
  REG_TYPE               m_reg_type;
  vector<Variable>       m_variables;
  vector<SampleEntry>    m_mc_entries;
  SampleEntry            m_data_entry;
  vector<string>         m_regions;
  vector<string>         m_save_type;
  Variable               m_current_var;
  string                 m_current_reg;
  string                 m_data_fmt;
  string                 m_mc_fmt;
  string                 m_save_path;
  bool                   m_draw_local;
  TFile *                m_data_file;
  TFile *                m_mc_file;
  string                 m_ftag;
};

DrawBox::DrawBox(TFile * file)
{
  m_data_file = file;
  m_mc_file = file;
  SetAtlasStyle();
}
DrawBox::DrawBox(TFile * dt_file,TFile *mc_file)
{
  m_data_file = dt_file;
  m_mc_file = mc_file;
  SetAtlasStyle();
}
void DrawBox::PrintYields()
{
  double y_mc,y_sum,y_dt;
  double e_mc,e_sum,e_dt;
  int N = m_current_sum_hist->GetNbinsX();
  y_sum = m_current_sum_hist->IntegralAndError(0,N,e_sum);
  y_dt = m_current_data_hist->IntegralAndError(0,N,e_dt);
  for (auto sam : m_current_mc_hists)
  {
    y_mc = sam.second->IntegralAndError(0,N,e_mc);
    //printf("%18s  %10.1f +- %8.1f\t\tratio %6.2f\n",(sam.first).c_str(),y_mc,e_mc,100*y_mc/y_sum);
    printf("%18s  %10.1f +- %8.1f %6.2f\n",(sam.first).c_str(),y_mc,e_mc,100*y_mc/y_sum);
  }
  
  printf("%18s  %10.1f +- %8.1f\n","Total mc",y_sum,e_sum);
  printf("%18s  %10.1f +- %8.1f\n","Data",y_dt,e_dt);
  printf("DATA over MC  %4.3f +- %4.3f\n",y_dt/y_sum,(y_dt/y_sum)*sqrt(pow(e_dt/y_dt,2)+pow(e_sum/y_sum,2))); 
}
void DrawBox::AddGlobeText(float x,float y,Color_t color,string text,float size = -1.)
{
  Text t = {x,y,color,text,size};
  m_globe_texts.push_back(t);
}
void DrawBox::AddLocalText(float x,float y,Color_t color,string text,float size = -1.)
{

  Text t = {x,y,color,text,size};
  m_local_texts.push_back(t);
}
void DrawBox::Set_ATLAS_LABEL(float x,float y,Color_t color)
{
  Text t = {x,y,color,"",-1.};
  m_atlas_label = t;
}
void DrawBox::ResetA()
{
  m_mc_entries.clear();
  m_data_entry = SampleEntry();
  ResetP();
}
void DrawBox::ResetP()
{
  m_variables.clear();
  m_local_texts.clear();
  m_regions.clear();
  m_data_fmt = "";
  m_mc_fmt = "";
  m_lp.x1 = 0.7;
  m_lp.y1 = 0.75;
  m_lp.x2 = 0.85;
  m_lp.y2 = 0.95;
  m_ftag = "";
  UserProcess = [](TH1F* h,UserDef::TYPE type){return h;};
}
void DrawBox::AddMC(string name,vector<string> keys,int color, float sf = 1.)
{
  m_mc_type   = SINGLE_KEY;
  SampleEntry entry = {name,"",keys,color,sf,MC,NULL};
  m_mc_entries.push_back(entry);
}
void DrawBox::AddMC(string name,vector<string> keys,int color, TFile* f, float sf = 1.)
{
  m_mc_type   = SINGLE_KEY;
  SampleEntry entry = {name,"",keys,color,sf,MC,f};
  m_mc_entries.push_back(entry);
}

void DrawBox::AddMC(string name,string key,vector<string> keys,int color, float sf = 1.)
{
  m_mc_type     = DOUBLE_KEY;
  SampleEntry entry   = {name,key,keys,color,sf,MC,NULL};
  m_mc_entries.push_back(entry);
}
void DrawBox::AddMC(string name,string key,vector<string> keys,int color, TFile* f,float sf = 1.)
{
  m_mc_type     = DOUBLE_KEY;
  SampleEntry entry   = {name,key,keys,color,sf,MC,f};
  m_mc_entries.push_back(entry);
}

//void AddMC(string name,string key,int color,float sf = 1.)
//{
//  m_mc_type   = DrawBox::MC_TYPE::SINGLE_KEY;
//  auto it = std::find_if(m_mc_entries.begin(),m_mc_entries.end(),[&name](DrawBox::SampleEntry entry) {return entry.name == name;});
//  if(m_mc_entries.end()==it)
//  {
//    vector<string> ks = {key};
////    AddMC(name,ks,color,sf);
//  }
//  else
//  {
//    (it->sub).push_back(key);
//  }
//}

void DrawBox::AddData(string name,string key,int color, float sf = 1.)
{
  SampleEntry entry;
  entry.name   = name;
  entry.key    = key;
  entry.color  = color;
  entry.sf     = sf;
  entry.type   = DATA;
  m_data_entry = entry;
}

void DrawBox::AddRegion(string reg)
{
  m_regions.push_back(reg);
}

void DrawBox::AddRegion(vector<string> regs)
{
  m_regions = regs;
}

void DrawBox::FillVariable(string name,string xtitle,string ytitle,bool log_scale,int nbins,double *xbins,float lowerL,float upperL,float funit,REBIN_TYPE rebin_type)
{
  Variable variable = {name,xtitle,ytitle,log_scale,nbins,xbins,lowerL,upperL,funit,rebin_type,ytitle};
  m_variables.push_back(variable);

}

void DrawBox::AddVariable(string var,string xtitle,string ytitle,bool log_scale,double *xbins,int nbins,float funit )
{
  FillVariable(var,xtitle,ytitle,log_scale,nbins,xbins,NAN,NAN,funit,RESPLIT);
}

void DrawBox::AddVariable(string var,string xtitle,string ytitle,bool log_scale,float lowerL,float upperL,float funit = -1.)
{
  FillVariable(var,xtitle,ytitle,log_scale,-1,NULL,lowerL,upperL,funit,RERANGE);
}

void DrawBox::AddVariable(string var,string xtitle,string ytitle,bool log_scale,int nbins=-1,float lowerL=NAN, float upperL=NAN,float funit = -1.)
{
  REBIN_TYPE rebin_type;
  if (nbins==-1) rebin_type = NOREBIN;
  else if(isnan(lowerL)) rebin_type = MERGE;
  else rebin_type = MERGE_RERANGE;
  FillVariable(var,xtitle,ytitle,log_scale,nbins,NULL,lowerL,upperL,funit,rebin_type);
}


void DrawBox::SetFormat(string fmt)
{
  m_data_fmt = fmt;
  m_mc_fmt = fmt;
}

void DrawBox::SetFormat(string dt_fmt,string mc_fmt)
{
  m_data_fmt = dt_fmt;
  m_mc_fmt = mc_fmt;
}

void DrawBox::SetSaveType(vector<string> save_type)
{
  m_save_type = save_type;
}

void DrawBox::SetSavePath(string path)
{
  m_save_path = path;
}
void DrawBox::Draw()
{

  if(m_regions.size()==0)
  {
    m_regions.push_back("");
    m_reg_type = NO_REG;
  }
  else 
  {
    m_reg_type = MULTI_REGS;
  }
  for(string reg : m_regions )
  {
    m_current_reg = reg;
    for(Variable var : m_variables)
    {
      m_current_var = var;
      DrawPic();  
    }
  }
}

void DrawBox::SetFile(TFile *df, TFile* mf)
{
  m_data_file = df;
  m_mc_file   = mf;
}

void DrawBox::Assert(void* h,string info = "")
{
  if(NULL==h)
  {
    cout<<info<<endl;
    throw 0;
  }
}

void DrawBox::DrawPic()
{
  map<string,TH1F *>   mc_hists;
  TH1F *               data_hist = NULL;
  TH1F *               sum_hist  = NULL;
  TH1F *               temp_hist = NULL;
  
  data_hist = GetHist(m_data_entry);
  data_hist = UserProcess(data_hist,UserDef::TYPE::DATA);
  PH(data_hist,"data hist");
  for (SampleEntry mc_entry : m_mc_entries)
  {
    string name = mc_entry.name;
    temp_hist  = GetHist(mc_entry);
    if(NULL==temp_hist)
      continue;
    PH(temp_hist,name);
    temp_hist  = UserProcess(temp_hist,UserDef::TYPE::MC_COM);
    mc_hists[name] = temp_hist;
    if(NULL==sum_hist)
    {
      sum_hist = (TH1F*)temp_hist->Clone();
    }
    else
    {
      sum_hist->Add(temp_hist);
    }
  }
  sum_hist = UserProcess(sum_hist,UserDef::TYPE::MC_SUM);
  m_current_mc_hists  = mc_hists;
  m_current_data_hist = data_hist;
  PH(m_current_data_hist,"m_curt_dt_hist");
  m_current_sum_hist  = sum_hist;
  PrintYields();
  GetPlots();
}

void DrawBox::GetPlots()
{
  string c_name = GetName(DATA,"DrawBox_");
  c_name.erase(0,c_name.find("/")+1);
	TCanvas* canvas = new TCanvas(c_name.c_str(),c_name.c_str(),800.,800.);

	THStack* hs = new THStack("hs",c_name.c_str());
	TPad* p_u = new TPad("pu","pu",0,0.3,1,1);
	TPad* p_d = new TPad("pd","pd",0,0,1,0.3);
	if(m_current_var.log_scale) p_u->SetLogy();
	//p_u->SetBottomMargin(0.01);
	p_u->SetBottomMargin(0.02);
	p_u->SetTopMargin(0.05);
	p_u->SetRightMargin(0.08);

	p_d->SetTopMargin(0.03);
	p_d->SetBottomMargin(0.3);
	p_d->SetRightMargin(0.08);

	p_u->Draw();
	p_d->Draw();
 
  double maxValue = max(m_current_data_hist->GetMaximum(), m_current_sum_hist->GetMaximum());
  
	//TLegend* leg = new TLegend(0.65,0.75,0.90,0.95);
	TLegend* leg = new TLegend(m_lp.x1,m_lp.y1,m_lp.x2,m_lp.y2);
	//TLegend* leg = new TLegend(0.70,0.75,0.85,0.95);
  int Nentries = m_mc_entries.size();
  if(Nentries>=5)
    leg->SetNColumns(1+int(Nentries/4));
	leg->SetFillStyle(0);
	leg->SetBorderSize(0);
	leg->AddEntry(m_current_data_hist, m_data_entry.name.c_str(), "p");
	
  for(SampleEntry entry : m_mc_entries)
  {
    auto it = m_current_mc_hists.find(entry.name);
    if(m_current_mc_hists.end() != it)
    {
	    leg->AddEntry(it->second,(it->first).c_str(),"f");
    }
  }
  auto it = m_mc_entries.rbegin();
  auto end = m_mc_entries.rend();
  for(;it!=end;++it)
  {
    auto it_map = m_current_mc_hists.find(it->name);
    if(m_current_mc_hists.end() != it_map)
    {   
      hs->Add(it_map->second);
    }   
  }
	leg->Draw();

	// draw the ATLAS label
	ATLAS_LABEL(m_atlas_label.x, m_atlas_label.y, m_atlas_label.color);
  for(auto T : m_globe_texts)
  {
    DrawText(T.x,T.y,T.color,T.text.c_str(),T.size);
  }
  for(auto T : m_local_texts)
  {
    DrawText(T.x,T.y,T.color,T.text.c_str(),T.size);
  }

	m_current_data_hist->SetMarkerSize(0.8);
	p_d->cd();
	TH1F* ratio =(TH1F*)m_current_data_hist->Clone();
	ratio->Reset();
	ratio->Divide(m_current_data_hist,m_current_sum_hist);
	TH1F* ratio_err_band =(TH1F*)m_current_sum_hist->Clone();
  int n = ratio_err_band->GetNbinsX();
  for(int i=1;i<=n;i++)
  {
    float bin_err = ratio_err_band->GetBinContent(i);
    float bin_content = ratio_err_band->GetBinError(i);
    float bin_content_dt = m_current_data_hist->GetBinContent(i); 
    ratio_err_band->SetBinContent(i,1);
    ratio_err_band->SetBinError(i,abs(bin_content/(bin_content-bin_err)-bin_content/(bin_content+bin_err)));
    if((bin_content_dt<0.1)&&(bin_content<0.1))ratio_err_band->SetBinError(i,0); 
  }
  ratio_err_band->SetFillStyle(3004);
  ratio_err_band->SetFillColor(kRed);
  ratio_err_band->SetMarkerSize(0.);
  ratio_err_band->SetMarkerColor(kRed);
  ratio_err_band->SetStats(0);
  ratio_err_band->SetLineWidth(1);
  p_d->cd();
  ratio_err_band->GetYaxis()->SetRangeUser(0.5,1.5);
  ratio_err_band->GetYaxis()->SetNdivisions(4,1,5);
	ratio_err_band->GetYaxis()->SetLabelSize(0.08);
	ratio_err_band->GetXaxis()->SetLabelSize(0.1);
	ratio_err_band->GetXaxis()->SetTitle(m_current_var.xtitle.c_str());
  ratio_err_band->GetXaxis()->SetTitleSize(0.1);
  ratio_err_band->Draw("E2same");
  
  TLegend *ratio_leg =  new TLegend(0.27,0.8,0.62,0.955);
  //TLegend *ratio_leg =  new TLegend(0.60,0.7,0.9,0.85);
  ratio_leg->AddEntry(ratio_err_band,"MC stat. uncertainty","f"); 
 	
  
	ratio->GetYaxis()->SetRangeUser(0.5,1.5);
	ratio->GetYaxis()->SetNdivisions(4,1,5);
	ratio->GetYaxis()->SetLabelSize(0.08);
	ratio->GetXaxis()->SetLabelSize(0.1);
	ratio->SetMarkerSize(0.6);
	ratio->Draw("same");



	double xmin = ratio->GetXaxis()->GetXmin();
	double xmax = ratio->GetXaxis()->GetXmax();

	//Tl->DrawLatex(xmin-xmax/10.0,1,"#frac{data}{MC}");	
	TLatex Tl(xmin-xmax/10.0,0.75,"Data/MC");
	Tl.SetTextAngle(90);
	Tl.SetTextSize(0.1);
	Tl.Draw();
  ratio_leg->Draw("same");

	TLine* L_One = new TLine(xmin,1,xmax,1);
	L_One->SetLineStyle(2);
	L_One->SetLineColor(1);
	L_One->SetLineWidth(2);
	L_One->Draw();
	p_u->cd();

	// set the maximum value of the y-axis
	float plot_sf=1.5;
	if ( m_current_var.log_scale ){
	   float x_min = 1;
	   hs->SetMaximum( TMath::Power(maxValue/x_min, plot_sf) * x_min);
	   hs->SetMinimum( x_min );
	} else {
	   hs->SetMaximum( plot_sf*maxValue );
	   hs->SetMinimum( 0. );
	}
	hs->Draw("HIST");
	hs->GetYaxis()->SetTitle(m_current_var.hs_ytitle.c_str());
	hs->GetYaxis()->SetTitleSize(0.05);
	hs->GetXaxis()->SetLabelOffset(999);
	m_current_data_hist->GetYaxis()->SetTitle(m_current_var.ytitle.c_str());
	m_current_data_hist->Draw("esame");
  for (string save_type : m_save_type)
  {
    string sname = m_save_path + "/" + c_name + m_ftag + "." + save_type;
	  canvas->Print(sname.c_str());
  }
	if(m_draw_local)canvas->Draw();
}

string DrawBox::GetName(SAMPLE_TYPE type,string key,string sub="")
{
  string name;
  if(DATA==type)
  {
    if(NO_REG == m_reg_type) 
    {
      name = (boost::format(m_data_fmt) % key % m_current_var.name).str();
    }
    else 
    {
      name = (boost::format(m_data_fmt) % key % m_current_reg % m_current_var.name).str();
    }
  }
  else
  {
    switch(m_reg_type + m_mc_type) 
    {
      case 0:
        name = (boost::format(m_mc_fmt) % sub % m_current_var.name ).str();
        break;
      case 1:
        name = (boost::format(m_mc_fmt) % key % sub % m_current_var.name ).str();
        break;
      case 2:
        name = (boost::format(m_mc_fmt)% sub % m_current_reg % m_current_var.name ).str();
        break;
      case 3:
        name = (boost::format(m_mc_fmt) % key % sub % m_current_reg % m_current_var.name ).str();
        break;
      default:
        perror("case_ should be 0-3");
    }
  }
  return name;
}
TH1F* DrawBox::GetHist(SampleEntry entry)
{
  string info;
  TH1F * sum = NULL;
  if(DATA==entry.type)
  {
    string name = GetName(entry.type,entry.key);
    info += name;
    sum = (TH1F*)m_data_file->Get(name.c_str());
    Assert(sum,"Data is empty:\n"+name);
    sum = Process(sum,entry);
    return sum;
  }
  else //MC
  {
    TFile * temp_f = NULL;
    if(NULL==entry.file)
      temp_f = m_mc_file;
    else
      temp_f = entry.file;

    for (string sub : entry.sub)
    {
      int case_ = m_mc_type + m_reg_type;
      string name = GetName(entry.type,entry.key,sub);
      info += "\n" + name;
      TH1F * temp = (TH1F*)temp_f->Get(name.c_str());
      if(NULL==temp)
        continue;
      if(NULL==sum){
        sum = (TH1F*)temp->Clone();
      } else {
        sum->Add(temp);
      }
    }
    try
    {
      Assert(sum,"\nEmpty: sum \n"+info);
      sum = Process(sum,entry);
    }
    catch(int a)
    {
      cout<<"skip this mc:"<<entry.name<<endl;
    }
  }
  return sum;
}
TH1F* DrawBox::Process(TH1F *h, SampleEntry entry)
{
  if(entry.type == MC)
    h->SetFillColor(entry.color);
  h->Scale(entry.sf);
  h->SetStats(0);
  int nbins;
  double * xbins;
  switch(m_current_var.rebin_type)
  {
    case NOREBIN:
      break;
    case MERGE:
      h->Rebin(m_current_var.nbins);
      break;
    case RESPLIT:
      nbins = m_current_var.nbins;
      xbins = m_current_var.xbins;
      h = (TH1F*)h->Rebin(nbins,"",xbins);
      break;
    case RERANGE:
      h->GetXaxis()->SetRangeUser(m_current_var.lowerL,m_current_var.upperL);
      break;
    case MERGE_RERANGE:
      h->Rebin(m_current_var.nbins);
      h->GetXaxis()->SetRangeUser(m_current_var.lowerL,m_current_var.upperL);
      break;
    default:
      perror("case overflowed in Variable.rebin_type");
  }
   
  h->GetXaxis()->SetTitle(m_current_var.xtitle.c_str());

  float funit = m_current_var.funit;
  if(funit<0)
  {
    char x[100];
    sprintf(x,m_current_var.ytitle.c_str(),h->GetBinWidth(1));
    h->GetYaxis()->SetTitle(x);
    m_current_var.hs_ytitle = string(x);
  }
  else
  {
    int n = h->GetNbinsX();
    for (int i=1;i<=n;i++)
    {
      float bin_c = h->GetBinContent(i);   
      float bin_e = h->GetBinError(i);
      float bin_w = h->GetBinWidth(i);
      h->SetBinContent(i,bin_c/(bin_w/funit));
      h->SetBinError(i,bin_e/(bin_w/funit));
    }
  }
  return (TH1F*)h->Clone();
}
void DrawBox::SetLegendPosition(float x_min,float y_min,float x_len, float y_len)
{
  m_lp.x1 = x_min;
  m_lp.x2 = x_min + x_len;
  m_lp.y1 = y_min;
  m_lp.y2 = y_min + y_len;
}
void DrawBox::DrawText(Double_t x,Double_t y,Color_t color, const char *text, Double_t tsize = -1){

  TLatex l;
  if(tsize>0)
    l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);
}
void DrawBox::SetUserProcess( TH1F* (*fun)(TH1F*,UserDef::TYPE) )
{
  UserProcess = fun;
}
#endif
