#!/usr/bin/python
import sys
import pickle
from ROOT import *

# import the plotting tool
sys.path.append('AnalysisToolKit/plotting')
import comparison as P

if __name__ == '__main__':

  # read DataPool from file
  DPfile = '../data/CaljetJetv02-04-17-1__XMu.DP.pickle' # 27.65

  if DPfile.find('Cal') !=-1: 
     JetName = 'Calo Jets'
     #mva_wps  = [ '85', ]
  elif DPfile.find('Trk') !=-1: 
     JetName = 'TrackJets'
     #mva_wps  = [ '75', ]

  #mva_wps  = [ '40', '45', '50', '55', '60', '65', '70', '75', '80', '85', '90', '100']
  #btag_wps = [ '60', '70', '77', '85']
  #mva_wps  = [ '60', '65', '70', '75', '80', '85', '90', '95', '100']
  btag_wps = ['77']
  #mva_wps  = ['75']

  # retrieve datapool for TP with MVAs
  DataPool = {}
  DataPool[JetName]={}
  dp_slice = pickle.load( open(DPfile,'rb') )
  print dp_slice
  DataPool[JetName] = dp_slice

  # prepare drawing
  #P.lumi = '36.5'
  P.lumi = '28'
  P.MethodSet.clear()
  P.PlotTitle = 'Comparison across MVA cuts'

  P.MethodSet = {
        #'75MVA':{'line':-1,'shift':0,  'colo':1},
        '85MVA':{'line':-1,'shift':0,  'colo':1},
        #'70MVA':{'line':-1,'shift':0,  'colo':1},
        '75MVA':{'line':-1,'shift':0,  'colo':1},
  }
  P.ErrSet  = {
        'total':{'colo':-1,'pos':0, 'line':1},
         'stat':{'colo':-1,'pos':0, 'line':1},
  }

  # decide the Method
  if JetName is 'Calo Jets':
     Method = '85MVA'
  else :
     Method = '75MVA'

  # draw scale factors with total syst
  config = {  'name':'sf',
              'OutName':'scalefactor',
                'title':'Scale factors',
             'DrawLine':True,
                   'LP':[0.60,0.20,0.93,0.35],
                'width':800,
               'height':800,
           'LegColumns':1,
              'LabSize':0.04,
	      'Status:':'Preliminary',
	      #'Status:':'Internal',
                 'Text':[ ['MV2c10, #varepsilon_{b} = 77%',[0.2,0.30]], [JetName,[0.2,0.23]] ],
               'YTitle':'#varepsilon_{b}^{data}/#varepsilon_{b}^{MC}',
               'XTitle':'Jet p_{T} [GeV]',
            }
  P.ErrSet  = {
     'total':{'colo':-1,'pos':0, 'line':1},
      'stat':{'colo':-1,'pos':1, 'line':1},
  }

  P.Central.update(config)
  for wp in btag_wps:
     P.Central['Text'] = [ ['MV2c10, #varepsilon_{b} = '+wp+' %',[0.2,0.30]], [JetName,[0.2,0.23]] ]
     P.MakeSFPlot(Method, JetName, wp, ['total','stat'], DataPool, True )

  # draw efficiency with total syst
  P.Central['title'] = 'Efficiency'
  P.Central['OutName'] = 'eff'
  P.Central['YTitle'] = 'b-jet efficiency'
  P.ErrSet  = {
      'e_dttotal':{'colo':kBlack,'pos':1, 'line':1},
     'e_mcmcstat':{'colo':kRed,'pos':0, 'line':1},
  }
  P.Central["DrawLine"] = False
  for wp in btag_wps:
     P.Central['Text'] = [ ['MV2c10, #varepsilon_{b} = '+wp+' %',[0.2,0.30]], [JetName,[0.2,0.23]] ]
     P.MakeEFFPlot( Method, [['e_dte_dt', 'e_dttotal'],['e_mc', 'e_mcmcstat']], JetName, wp, DataPool, True )
