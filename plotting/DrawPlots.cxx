#include <vector>
#include <iostream>
#include <string>
#include <map>
#include <iomanip>
#include <cstdlib>
#include <DrawBox.h>
using namespace std;

TH1F* ScaleMu(TH1F* h,UserDef::TYPE type)
{
  if(type==UserDef::TYPE::DATA)
  {
    TH1F *temp = (TH1F*)h->Clone(); 
    temp->Reset();
    int N = h->GetNbinsX();
    for(int n=0;n<=N;n++)
    {
      float binc = h->GetBinContent(n);
      float bine = h->GetBinError(n);
      temp->SetBinContent(int(n/1.09+0.5),binc);
      temp->SetBinError(int(n/1.09+0.5),bine);
    }
    return temp;
  }
  else
  {
    return h;
  }
}
void DrawPlots()
{
  string jet = "Cal";

  // set the optimized mva cut point
  string mva = "85";
  if(jet=="Trk")
    mva = "75";
  //overall info
  TFile* input = TFile::Open(("/eos/atlas/user/c/changqia/BTagging/"+jet+"Jetv02-06-17-1/input.root").c_str());
  DrawBox* drawBox = new DrawBox(input);
  drawBox->SetSavePath("./plots/");
  //drawBox->SetSavePath("./test_draw_box/");

  // setting the format of the output plots
  drawBox->SetSaveType({"png"});
  // if true show the plots in the interactive mode
  drawBox->SetLocalDraw(false);

  // set the global information energy, status, luminosity
  drawBox->Set_ATLAS_LABEL(0.20, 0.90, 1); 
  drawBox->AddGlobeText(0.37, 0.90, 1, "#font[42]{Internal}");
  drawBox->AddGlobeText(0.20, 0.85, 1, "#font[72]{#sqrt{s}=13 TeV, 37 fb^{-1}}",0.03);
 

  // drawing from the Nominal or varivation? and the region
  drawBox->SetFormat(("SysNominal/%1%_TP_1ptag2jet_MVA"+mva+"_XMu_em_%2%%3%").c_str());
  drawBox->AddLocalText(0.20, 0.80, 1, "#font[72]{e #mu 2 jets , #geq 1 tagged}",0.03);
  // add the MC processes
  drawBox->AddMC("t#bar{t}",{"ttbarPy8"},kWhite);
  //drawBox->AddMC("Diboson",{"diboson_sherpa"},kYellow);
  drawBox->AddMC("Single top",{"stop_Wt","stop_s","stop_t"},kRed);
  drawBox->AddMC("Z+jets", {"MadZee","MadZmumu","MadZtautau"},kBlue);
  drawBox->AddData("data","data",1);
  // setting the rebin
  double xbins_pt_Cal[] = {0,20,30,60,90,140,200,300};//calo jet
  double xbins_pt_Trk[] = {0,10,20,30,60,100,250};//track jet
  double xbins_eta[13] ;
  for(int n=0;n<13;n++)
  {
    xbins_eta[n] = -3.0 + 0.5*n;
  }
  double xbins_mv2c10[21];
  for (int n=0;n<21;n++)
  {
    xbins_mv2c10[n] = -0.6 + n*0.05;
  }
  double * xbins_pt;
  int nbins_pt;
  if(jet=="Cal"){
    xbins_pt = xbins_pt_Cal;
    nbins_pt = length(xbins_pt_Cal) -1 ;
  } else {
    xbins_pt = xbins_pt_Trk;
    nbins_pt = length(xbins_pt_Trk) -1 ;
  }
  // add the variables
  //drawBox->AddVariable(jet+"JetPhi","#phi","Jets / %.1f",true);
  //drawBox->AddVariable(jet+"JetPt","p_{T}(jet) [GeV]","Jets / 10 GeV",true,xbins_pt,nbins_pt,10.);
  drawBox->AddVariable("Ntagged","N tagged","Number of Jets",false,1);
  //drawBox->AddVariable(jet+"JetEta","#eta","Jets / 0.5",false,xbins_eta,length(xbins_eta)-1,0.5);
  //drawBox->AddVariable(jet+"JetMV2c10","MV2c10","Jets / %.2f",false);
  vector<string> reg = {""};
  //vector<string> reg = {"PxT60","PxT70","PxT77","PxT85"};
  drawBox->AddRegion(reg);
  drawBox->Draw();

  drawBox->ResetP();
  drawBox->SetFormat("SysNominal/%1%_TP_1ptag2jet_MVA"+mva+"_XMu_em_%2%");
  drawBox->AddLocalText(0.20, 0.80, 1, "#font[72]{e #mu 2 jets , #geq 1 tagged}",0.03);
  double xbins_mll[26];
  for(int n=0;n<26;n++)
  {
    xbins_mll[n] = 20*n;
  }
  double xbins_bdt[22]; 
  for(int n=0;n<22;n++)
  {
    xbins_bdt[n] = -0.65 + n*0.05;
  }
  
 // drawBox->AddVariable("Mll","M_{ll}","Entries / 20. GeV", false,xbins_mll,25,20.);
  drawBox->AddVariable("BDT","BDT output","Entries / 0.05",false,xbins_bdt,21,0.05);
  drawBox->SetFileTag("_sam_py8");
  drawBox->Draw();
 
  drawBox->ResetP();
  drawBox->SetUserProcess(&ScaleMu);
  drawBox->SetFormat("SysNominal/%1%_%2%_%3%");
  drawBox->AddLocalText(0.20, 0.80, 1, "#font[72]{Pre selection, #geq 0 jets}",0.03);
  drawBox->AddVariable("mu","#LT #mu #GT","Entries / %.0f",false);
  drawBox->AddRegion("S2_pretag2jet_preMVA_XMu_em");
  drawBox->AddRegion("TP_1ptag2jet_MVA85_XMu_em");
  drawBox->Draw();



  drawBox->ResetP();
  drawBox->SetUserProcess(&ScaleMu);
  drawBox->SetFormat("SysNominal/%1%_%2%_%3%");
  drawBox->AddLocalText(0.20, 0.80, 1, "#font[72]{e #mu 2 jets, #geq 1 tagged}",0.03);
  drawBox->AddVariable("mu","#LT #mu #GT","Entries / %.0f",false);
  drawBox->AddRegion("TP_1ptag2jet_MVA85_XMu_em");
  //drawBox->Draw();


  drawBox->ResetA();
  string mc_fmt = "SysNominal/%2%_%1%_TP_1ptag2jet_MVA"+mva+"_XMu_em_%3%";
  string dt_fmt = "SysNominal/%1%_TP_1ptag2jet_MVA"+mva+"_XMu_em_%2%";
  drawBox->SetLegendPosition(0.65,0.75,0.25,0.2);
  drawBox->SetFormat(dt_fmt,mc_fmt);
  drawBox->AddLocalText(0.20, 0.80, 1, "#font[72]{e #mu 2 jets , #geq 1 tagged}",0.03);
  vector<string> flav = {"bb","bc","bl","cc","cl","ll"};
  vector<int> color =   {kWhite,kBlue,kRed,kOrange,kGreen,kYellow};
  vector<string> sample = {"ttbarPy8","diboson_sherpa","stop_Wt","stop_s","stop_t","MadZee","MadZmumu","MadZtautau"};
  for(int n=0;n<6;n++)
  {
    drawBox->AddMC(flav[n],flav[n],sample,color[n]);
  }
  drawBox->AddData("data","data",1);
  drawBox->AddVariable("BDT","BDT output","Entries / 0.05",false,xbins_bdt,21,0.05);
  drawBox->SetFileTag("_flav_py8");
  drawBox->Draw();
}
