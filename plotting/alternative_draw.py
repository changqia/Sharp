from ROOT import *
import operator
import array
from AtlasStyle import *
gROOT.SetBatch(1)
save_dir = './plots'
runtag = 'py6mva100'
sf = 1.
#sf = 36.5/27.65
Samples = {
              'tt'        :          ['ttbar'],
              'diboson'   :          ['diboson_sherpa'],
              'z+jets'    :          ['MadZee','MadZmumu','MadZtautau'],
              'stop'      :          ['stop_s','stop_t','stop_Wt'],
}

alter_tt = [
              'tt'  ,   [ 
                         #( 'name in the legend',   ['samplename'],           color,  ) ,
                          ( 'tt Pythia6'        ,   ['ttbar'],                kBlack, ) ,   
                          ( 'tt Py6 radhi'      ,   ['ttbar_radhi'],          kRed,   ) ,  
                          ( 'tt Py6 radlo'      ,   ['ttbar_radlo'],          kYellow,) ,
                          ( 'tt PowHW'          ,   ['ttbarPowHW_UEEE5'] ,    kGreen, ) ,
                          ( 'tt aMC@NloHW'      ,   ['ttbaraMcAtNloHW'],      kRed+1, ) ,
                        ]
]
f = TFile('/eos/atlas/user/c/changqia/BTagging/CalJetFeb.17.2017.01/input.root')
#f = TFile('/eos/atlas/user/c/changqia/BTagging/CalJetv02-06-17-1/input.root')
def Out(hist, name):
  n = hist.GetNbinsX()
  print '{0:<16}'.format(name),
  for i in range(1,n+1):
    print '{0:<5}'.format('{0:.1f}'.format(hist.GetBinContent(i))),
  print 

def GetAlters(alter,fmt,xbins):
  ex_samples = []
  for sam in Samples:
    if alter[0] != sam:
      ex_samples += Samples[sam]
  alt_mc = {} 
  for alt,alts,color in alter[1]:
      print 'alt\n',alt,'\n'
      print 'ex + alts',ex_samples+alts,'\n\n'
      alt_mc[alt] = GetHists(ex_samples+alts,fmt,xbins)
      alt_mc[alt].SetLineColor(color)
      alt_mc[alt].SetMarkerColor(color)
      alt_mc[alt].SetStats(0)
  return alt_mc

def GetHists(sams,fmt,xbins):
  h = None
  for sam in sams:
    temp = f.Get(fmt.format(sam))
    if temp == None: 
      print 'not get {0:}'.format(fmt.format(sam))
      continue;
    print 'get {0:}'.format(fmt.format(sam))
    if  sam.find('data')==-1:
      print 'scale!'
      temp.Scale(sf)
    if h == None: 
      h = temp.Clone()
    else: 
      h.Add(temp)
  if h == None:
    print 'Nothing !!!!! \n',sams
    exit(0)
  h = h.Rebin(len(xbins)-1,'',array.array('d',xbins))
  h.SetMarkerSize(0)
  return h
def ATLAS_LABEL(x, y, color):
   l=TLatex()
   l.SetNDC()
   l.SetTextFont(72)
   l.SetTextColor(color)
   l.DrawLatex(x,y,"ATLAS")

def myText(x, y, color, text,size = None):
   l=TLatex() #l.SetTextAlign(12); l.SetTextSize(tsize); 
   if size != None: 
     l.SetTextSize(size)
   l.SetNDC()
   l.SetTextColor(color)
   l.DrawLatex(x,y,text)

def DrawLine(xmin,ypos,xmax,co):
   line = TLine(xmin, ypos, xmax, ypos)
   line.SetLineColor(co)
   line.SetLineStyle(2)
   line.SetLineWidth(3)
   return line

def Draw(alter,fmt,xbins,isLog,title,xtitle,ytitle='Events'):
  dataH = GetHists(['data'],fmt,xbins)
  alt_mc = GetAlters(alter,fmt,xbins)
  ratio_mc = {}
  for alter_item in alter[1]:
    alt   = alter_item[0]
    altH  = alt_mc[alt]
    color = alter_item[2]
    r = dataH.Clone()
    r.Reset()
    r.Divide(dataH,altH)
    r.SetLineColor(color)
    r.GetXaxis().SetTitle(xtitle);
    r.SetStats(0);
    r.GetYaxis().SetRangeUser(0.5,1.5);
    r.GetYaxis().SetNdivisions(-2);
    r.GetYaxis().SetLabelSize(0.08);
    r.GetXaxis().SetTitleSize(0.1);
    r.GetXaxis().SetLabelSize(0.1);
    r.SetMarkerSize(0.6);
  
    ratio_mc[alt] = r
  
  maxs_m = {key:TH1F.GetMaximum(alt_mc[key]) for key in alt_mc}
  max_m = max(maxs_m.iteritems(),key=operator.itemgetter(1))
  max_d = dataH.GetMaximum()
  
  if max_d > max_m:
    max_value = max_d
  else:
    max_value = max_m[1]
  
  canvas = TCanvas(title,title,800,800)
  
  p_u = TPad("pu","pu",0,0.31,1,1)
  p_d = TPad("pu","pu",0,0.,1,0.29)  
  if isLog:
    p_u.SetLogy()
  p_u.SetBottomMargin(0.01);
  p_u.SetTopMargin(0.05);
  p_u.SetRightMargin(0.08);
  p_d.SetTopMargin(0.02);
  p_d.SetBottomMargin(0.3);
  p_d.SetRightMargin(0.08);  
  
  p_d.Draw()
  p_u.Draw()

  leg = TLegend(0.70,0.70,0.85,0.95)  
  leg.SetFillStyle(0)
  leg.SetBorderSize(0)
  for alt in alter[1]:
    leg.AddEntry(alt_mc[alt[0]],alt[0],'l')
  leg.AddEntry(dataH,'data','p')
  leg.Draw()
  
  ATLAS_LABEL(0.20, 0.90, 1); 
  myText(0.40, 0.90, 1, "#font[42]{Internal}")
  myText(0.20, 0.85, 1, "#font[72]{#sqrt{s}=13 TeV, 36.5 fb^{-1}}",0.03)
  myText(0.20, 0.80, 1, "#font[72]{e #mu 2 jets , #geq 1 tagged}",0.03)

   
  dataH.SetStats(0);
  dataH.SetMarkerSize(0.8);  
   
  p_d.cd();

  for alt,alt_r in ratio_mc.iteritems():
    alt_r.SetMarkerSize(0)
    alt_r.Draw('same')
    Out(alt_r,'r {0:}'.format(alt))
  xmin = dataH.GetXaxis().GetXmin();
  xmax = dataH.GetXaxis().GetXmax();

  Tl = TLatex(xmin-xmax/10.0,0.75,"Data/MC");
  Tl.SetTextAngle(90);
  Tl.SetTextSize(0.1);
  Tl.Draw();

  L_One = TLine(xmin,1,xmax,1);
  L_One.SetLineStyle(2);
  L_One.SetLineColor(1);
  L_One.SetLineWidth(2);
  L_One.Draw();

  p_u.cd()

  for alt,altH in alt_mc.iteritems():
    plot_sf=1.5;
    if isLog:
       x_min = 1;
       altH.SetMaximum(  ((max_value/x_min) ** plot_sf) *x_min);
       altH.SetMinimum( x_min );
    else: 
       altH.SetMaximum( plot_sf*max_value );
       altH.SetMinimum( 0. );
    altH.Draw("same");
    Out(altH,alt)
    altH.GetYaxis().CenterTitle();
    altH.GetYaxis().SetTitle(ytitle);
    altH.GetYaxis().SetTitleSize(0.05);
    altH.GetXaxis().SetLabelOffset(999);
  dataH.GetYaxis().SetTitle(ytitle);
  dataH.Draw("esame");
  Out(dataH,'data')
  canvas.Draw();
  canvas.Print('{0:}/Alter_{1:}_{2:}.png'.format(save_dir,title,runtag))

def main():
  var_tag = {'S2':'','TP':'PxT77'}
  for reg in ['TP']:
    fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_'.format(reg)+var_tag[reg]+'CalJetPt'.format(reg)
    xbins = [0,20,30,60,90,140,200,300] 
    print fmt
    Draw(alter_tt,fmt,xbins,True,'C_{0:}_CalJetPt'.format(reg),'P_{T}(jet) [GeV]','Events')
    
    fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_'.format(reg)+var_tag[reg]+'CalJetEta'.format(reg)
    xbins = [-3.+x*0.5 for x in range(0,13)]
    Draw(alter_tt,fmt,xbins,True,'C_{0:}_CalJetEta'.format(reg),'\eta(jet)','Events')
    
    fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_'.format(reg)+var_tag[reg]+'CalJetPhi'.format(reg)
    xbins = [x*0.1 for x in range(0,33)]
    Draw(alter_tt,fmt,xbins,True,'C_{0:}_CalJetPhi'.format(reg),'\phi(jet)','Events')

    fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_'.format(reg)+var_tag[reg]+'CalJetMV2c10'.format(reg)
    xbins = [x*0.1 for x in range(-11,12)]
    Draw(alter_tt,fmt,xbins,False,'C_{0:}_CalJetMV2c10'.format(reg),'MV2c10(jet)','Events')
  

    fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_Mll'.format(reg)
    xbins = [x*25 for x in range(0,20)]
    Draw(alter_tt,fmt,xbins,False,'C_{0:}_Mll'.format(reg),'M(ll) GeV','Events')

  fmt = 'SysNominal/{{0:}}_{0:}_1ptag2jet_MVA85_XMu_em_BDT'.format('TP')
  xbins = [x*0.05 for x in range(-10,11)]
  Draw(alter_tt,fmt,xbins,False,'C_{0:}_BDT'.format(reg),'BDT weight','Events')



if __name__ == '__main__':
  main()
